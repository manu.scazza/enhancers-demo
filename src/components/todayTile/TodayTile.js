import styles from './todayTile.module.css';
import moment from 'moment-timezone';
import { useSelector } from 'react-redux';


const TodayTile = (props) => {
  const data = useSelector(state => state.weather.forecast);
  if (!data) return <div></div>
  return (
    <div className={styles.container}>
      <h1 className={styles.title}>Today</h1>
      <div className={styles.today} shadow={'true'}>
        <div className={styles.now}>Now</div>
        <div className={styles.nowTemp}>
          <span>{Math.round(data.hourly[0].temp)}&deg;</span>
        </div>
        {data.hourly.slice(1, 4).map(hourly => {
          return (
            <div key={hourly.dt} className={styles.hourly}>
              <span>{Math.round(hourly.temp)}&deg;</span>
              <span className={styles.time}>{moment(hourly.dt * 1000).tz(data.timezone).format('h a')}</span>
            </div>
          );
        })}
      </div>
    </div>
  )
};

export default TodayTile;