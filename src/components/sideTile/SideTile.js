import styles from './sideTile.module.css';
import { getColors, getIcon } from '../../lib/icons';
import moment from 'moment-timezone';
import { useDispatch } from 'react-redux';
import { update } from '../../store/index';

const SideTile = (props) => {
  const dispatch = useDispatch();
  const date = moment(props.data.dt * 1000 + props.data.timezone * 1000);
  console.log(props.data)
  const { from, to, font } = getColors(props.data.weather[0].id)
  return (
    <div
      className={styles.tile}
      style={{ background: `linear-gradient(180deg, ${from} 0%, ${to} 100%)`, color: `${font}` }}
      shadow={'true'}
      onClick={() => { dispatch(update(props.data)) }}
    >
      <div className={styles.info}>
        <h2>{props.data.name}</h2>
        <h5>{date.format('dddd D, MMMM')}</h5>
        <p>{date.format('h a')}</p>
      </div>
      <img src={getIcon(props.data.weather[0].id)} alt="" className={styles.icon} />
      <div>
        <p className={styles.temp}>{Math.round(props.data.main?.temp)}&deg;</p>
      </div>
    </div>
  );
};

export default SideTile;