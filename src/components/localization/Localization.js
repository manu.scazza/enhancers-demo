import styles from './localization.module.css';
import icon from '../../assets/images/location.png';
import { useDispatch } from 'react-redux';
import { update } from '../../store';
import { fetchData } from '../../lib/fetchData';

const Localization = (props) => {
  const dispatch = useDispatch();
  const handleClick = () => {
    if ("geolocation" in navigator) {
      navigator.geolocation.getCurrentPosition(position => {
        const params = {
          lat: position.coords.latitude,
          lon: position.coords.longitude,
          units: 'metric'
        }
        fetchData('weather', params)
          .then(data => dispatch(update(data)))
          .catch(err => console.error('Error: ', err));
      });
    } else {
      window.alert('Not available');
    }
  }
  return (
    <div className={styles.container}>
      <p>Localization</p>
      <div className={styles.panel} shadow={'true'} onClick={handleClick}>
        <img className={styles.icon} src={icon} alt="" />
        <p>Add localization</p>
      </div>
    </div>
  );
};

export default Localization;