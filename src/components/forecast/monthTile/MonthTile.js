import styles from './monthTile.module.css';
import icon from '../../../assets/images/wind_detail.png';
import moment from 'moment-timezone';

const WIND_CONDITION = {
  0: 'No wind',
  1: 'Windy',
  2: 'Strong wind',
  3: 'Dangerously windy',
  4: 'Tornado'
}

const MonthTile = (props) => {
  let wind_index = Math.round(props.data?.wind_speed / 10);
  return (
    <div className={styles.container} shadow={'true'}>
      <div className={styles.left}>
        <p>{moment(props.data?.dt * 1000).format('ddd, D MMM')}</p>
        <img src={icon} alt="" />
      </div>
      <div className={styles.right}>
        <div>
          <p className={styles.temp}>{Math.round(props.data?.temp.day)}&deg;</p>
          <p className={styles.condition}>{WIND_CONDITION[wind_index]}</p>
          <p>The high will be {Math.round(props.data?.temp.max)}&deg;C, the low will be {Math.round(props.data?.temp.min)}&deg;C.</p>
        </div>
        <div>
          <p>Humidity: {Math.round(props.data?.humidity)}%</p>
          <p>UV: {Math.round(props.data?.uvi)}</p>
          <p>Dew point: {Math.round(props.data?.dew_point)}&deg;C</p>
        </div>
      </div>
    </div>
  );
};

export default MonthTile;