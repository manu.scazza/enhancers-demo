import { useState } from 'react';
import styles from './forecast.module.css';
import SmallTile from './smallTile/SmallTile';
import MonthTile from './monthTile/MonthTile';
import { useSelector } from 'react-redux';

const VIEWS = { week: 0, month: 1 };

const Forecast = (props) => {
  const [view, switchView] = useState(VIEWS.week);
  const data = useSelector(state => state.weather.forecast);

  if (!data) return <div>Error</div>

  let content = <></>;
  if (view === VIEWS.week) {
    content = <>
      {data.daily.slice(1, 4).map(daily =>
        <SmallTile key={daily.dt} data={daily} timezone={data.timezone} />
      )}
    </>;
  } else if (view === VIEWS.month) {
    content = <MonthTile data={data.daily[0]} />
  }
  return (
    <div>
      <div className={styles.forecastHeader} shadow={'true'}>
        <div className={view === VIEWS.week ? styles.bgActive : ''} onClick={() => switchView(VIEWS.week)}>
          <h1>This week</h1>
        </div>
        <div className={view === VIEWS.month ? styles.bgActive : ''} onClick={() => switchView(VIEWS.month)}>
          <h1>This month</h1>
        </div>
      </div>
      <div className={styles.forecast} shadow={'true'}>
        {content}
      </div>
    </div>
  )
};

export default Forecast;