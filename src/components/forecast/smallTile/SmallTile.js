import styles from './smallTile.module.css';
import { getIcon } from '../../../lib/icons';
import moment from 'moment-timezone';

const SmallTile = (props) => {
  return <div shadow={'true'} className={styles.panel}>
    <h1>{moment(props.data?.dt * 1000).tz(props.timezone).format('dddd')}</h1>
    <p>{Math.round(props.data?.temp.day)}&deg;</p>
    <img src={getIcon(props.data?.weather[0].id)} alt="" className={styles.icon} />
  </div>
};

export default SmallTile;