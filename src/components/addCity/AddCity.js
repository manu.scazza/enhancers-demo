import styles from './addCity.module.css';
import addIcon from '../../assets/images/plus.png';

const AddCity = (props) => {
  return (
    <div className={styles.addBtn}>
      <div className={styles.btnText} onClick={props.handleAddCity}>
        <img src={addIcon} alt="" className={styles.addIcon} />
        Aggiungi citt&agrave;
      </div>
    </div>
  );
};

export default AddCity;