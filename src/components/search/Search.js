import { useState } from 'react';
import styles from './search.module.css';
import searchIcon from '../../assets/images/search.png';
import { fetchGeoData, fetchData } from '../../lib/fetchData';
import { useDispatch } from 'react-redux';
import { update } from '../../store';

const Search = (props) => {
  const dispatch = useDispatch();
  const [text, setText] = useState('');
  const handleInput = (el) => setText(el.target.value);
  const handleClick = () => {
    if (text === '') return;
    fetchGeoData(text).then(data => {
      if (data.length !== 0) {
        setText('');
        const params = {
          lat: data[0].lat,
          lon: data[0].lon,
          units: 'metric'
        }
        fetchData('weather', params)
          .then(data => dispatch(update(data)))
          .catch(err => console.error('Error: ', err));
      } else {
        window.alert('Nothing found.')
      }
    }).catch(err => console.error('Error: ', err));
  }
  return (
    <div className={styles.container}>
      <p>Search</p>
      <div className={styles.panel} shadow={'true'}>
        <input
          className={styles.textInput}
          type="text" value={text}
          placeholder={'ex: Miami'}
          onChange={handleInput}
          onKeyPress={(event) => {
            if (event.key === 'Enter')
              handleClick()
          }} />
        <div className={styles.iconContainer} onClick={handleClick}>
          <img src={searchIcon} alt="" className={styles.icon} />
        </div>
      </div>
    </div>
  );
};

export default Search;