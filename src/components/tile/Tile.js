import moment from 'moment-timezone';
import styles from './tile.module.css';
import { useSelector } from 'react-redux';
import { getIcon } from '../../lib/icons';

const Tile = (props) => {
  const weatherData = useSelector(state => state.weather.data);
  const additionalData = useSelector(state => state.weather.forecast);
  if (!weatherData || !additionalData) return <div></div>
  return (
    <div className={styles.tile} shadow={'true'}>
      <h2>{weatherData.name}</h2>
      <h3>{moment(additionalData.current?.dt * 1000).tz(additionalData.timezone).format('dddd D, MMMM')}</h3>
      <p>{weatherData.weather ? weatherData.weather[0].main : ''}</p>
      <div className={styles.temp}>
        <div className={styles.tempInner} shadow={'true'}>
          <h1>{Math.round(weatherData.main.temp)}&deg;</h1>
          <img src={getIcon(weatherData.weather[0].id)} alt={''}></img>
        </div>
      </div>
    </div>
  );
};

export default Tile;