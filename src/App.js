import { useEffect, useState } from 'react';
import Tile from './components/tile/Tile';
import SideTile from './components/sideTile/SideTile';
import TodayTile from './components/todayTile/TodayTile';
import Forecast from './components/forecast/Forecast';
import Search from './components/search/Search';
import styles from './app.module.css';
import Localization from './components/localization/Localization';
import AddCity from './components/addCity/AddCity';
import { fetchData } from './lib/fetchData';
import { useDispatch, useSelector } from 'react-redux';
import { update, updateForecast } from './store/index';

function App() {
  const [isLoading, setIsLoading] = useState(false);
  const [isOnError, setIsOnError] = useState(false);
  const [cities, setCities] = useState([]);
  const dispatch = useDispatch();
  const weatherData = useSelector(state => state.weather.data);

  useEffect(() => {
    const params = {
      lat: 45.0705,
      lon: 7.6868,
      units: 'metric'
    };
    fetchData('weather', params)
      .then(data => dispatch(update(data)))
      .catch(err => {
        console.error('Error: ', err);
        setIsOnError(true);
      })
      .finally(setIsLoading(false));
  }, [dispatch]);

  // setTimeout(()=>{dispatch(update)}, 2000)

  useEffect(() => {
    if (!weatherData?.coord) return;
    const params = {
      lat: weatherData.coord.lat,
      lon: weatherData.coord.lon,
      exclude: 'minutely,alerts',
      units: 'metric'
    };
    fetchData('onecall', params)
      .then(data => dispatch(updateForecast(data)))
      .catch(err => {
        console.error('Error: ', err);
        setIsOnError(true);
      })
      .finally(setIsLoading(false));
  }, [dispatch, weatherData]);

  const handleAddCity = () => {
    if (cities.find(el => el.name === weatherData.name)) {
      window.alert('Already present')
      return;
    }
    const newArr = [...cities];
    if (newArr.unshift(weatherData) > 2) {
      newArr.pop();
    }
    setCities(newArr);
  }

  if (isLoading)
    return <div>Loading...</div>;
  if (isOnError)
    return <div>Error loading data.</div>
  return (
    <div className={styles.mainPanel}>
      <Tile />
      <div className={styles.sidePanel}>
        <AddCity handleAddCity={handleAddCity} />
        {cities.map(city => {
          return <SideTile key={city.name} data={city} />
        })}
      </div>
      {/* bottom panels */}
      <div className={styles.bottomPanel}>
        <TodayTile />
        <Forecast />
      </div>
      <div className={styles.bottomRightPanel}>
        <Search />
        <Localization />
      </div>
    </div>
  );
}

export default App;
