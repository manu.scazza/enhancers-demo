const APP_ID = process.env.REACT_APP_APP_ID;
const BASE_URL = 'http://api.openweathermap.org/data/2.5';

const fetchData = async (segment, params) => {
  let query = `appid=${APP_ID}`;
  Object.keys(params).map(key => query += `&${key}=${params[key]}`);
  const fullUrl = `${BASE_URL}/${segment}?${query}`;

  const res = await fetch(fullUrl);
  if (!res.ok)
    throw new Error('Error fetching data: ', res.status);
  const data = await res.json();
  return data;
};

const fetchGeoData = async (name) => {
  const res = await fetch(`http://api.openweathermap.org/geo/1.0/direct?q=${name}&limit=1&appid=${APP_ID}`);
  if (!res.ok)
    throw new Error('Error fetching data: ', res.status);
  const data = await res.json();
  return data;
};

export { fetchData, fetchGeoData };