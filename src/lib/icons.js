import cloudy from '../assets/images/cloudy.png';
import lightRain from '../assets/images/light_rain.png';
import moderateRain from '../assets/images/moderate_rain.png';
import partlyCloud from '../assets/images/partly_cloud.png';
import sunny from '../assets/images/sunny.png';
import thunder from '../assets/images/thunder.png';
import snow from '../assets/images/snow.png';
import atmosphere from '../assets/images/atmosphere.png';
import noMsg from '../assets/images/no-message.png';

export const getIcon = (code) => {
  if (code >= 200 && code <= 232) return thunder
  if (code >= 300 && code <= 321) return moderateRain
  if (code >= 500 && code <= 531) return lightRain
  if (code >= 600 && code <= 622) return snow
  if (code >= 701 && code <= 781) return atmosphere
  if (code === 800) return sunny
  if (code === 801 || code === 802) return partlyCloud
  if (code === 803 || code === 804) return cloudy
  return noMsg
}

export const getColors = (code) => {
  if (code >= 200 && code <= 232) return { from: '#2c3e50', to: '#4ca1af', font: '#fff' }
  if (code >= 300 && code <= 321) return { from: '#1c92d2', to: '#f2fcfe', font: '#fff' }
  if (code >= 500 && code <= 531) return { from: '#0a2164', to: '#4b87cb', font: '#fff' }
  if (code >= 600 && code <= 622) return { from: '#fdfbfb', to: '#ebedee', font: '#011760' }
  if (code >= 701 && code <= 781) return { from: '#b2fefa', to: '#0ed2f7', font: '#fff' }
  if (code === 800) return { from: '#5374e7', to: '#77b9f5', font: '#fff' }
  if (code >= 801 || code <= 802) return { from: '#5374e7', to: '#77b9f5', font: '#fff' }
  if (code >= 803 || code <= 804) return { from: '#4f556d', to: '#8f9faf', font: '#fff' }
}