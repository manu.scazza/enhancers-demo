export const data = {
  "lat": 40.4085,
  "lon": 17.2805,
  "timezone": "Europe/Rome",
  "timezone_offset": 7200,
  "current": {
    "dt": 1626792431,
    "sunrise": 1626752240,
    "sunset": 1626805006,
    "temp": 29.74,
    "feels_like": 32.16,
    "pressure": 1008,
    "humidity": 59,
    "dew_point": 20.87,
    "uvi": 0.89,
    "clouds": 40,
    "visibility": 10000,
    "wind_speed": 1.34,
    "wind_deg": 245,
    "wind_gust": 5.36,
    "weather": [
      {
        "id": 802,
        "main": "Clouds",
        "description": "scattered clouds",
        "icon": "03d"
      }
    ]
  },
  "hourly": [
    {
      "dt": 1626789600,
      "temp": 29.7,
      "feels_like": 31.73,
      "pressure": 1009,
      "humidity": 57,
      "dew_point": 20.27,
      "uvi": 1.61,
      "clouds": 39,
      "visibility": 10000,
      "wind_speed": 2.05,
      "wind_deg": 257,
      "wind_gust": 4.4,
      "weather": [
        {
          "id": 802,
          "main": "Clouds",
          "description": "scattered clouds",
          "icon": "03d"
        }
      ],
      "pop": 0
    },
    {
      "dt": 1626793200,
      "temp": 29.74,
      "feels_like": 32.16,
      "pressure": 1008,
      "humidity": 59,
      "dew_point": 20.87,
      "uvi": 0.89,
      "clouds": 40,
      "visibility": 10000,
      "wind_speed": 1.61,
      "wind_deg": 222,
      "wind_gust": 3.28,
      "weather": [
        {
          "id": 802,
          "main": "Clouds",
          "description": "scattered clouds",
          "icon": "03d"
        }
      ],
      "pop": 0
    },
    {
      "dt": 1626796800,
      "temp": 29.45,
      "feels_like": 31.48,
      "pressure": 1009,
      "humidity": 58,
      "dew_point": 20.32,
      "uvi": 0.81,
      "clouds": 45,
      "visibility": 10000,
      "wind_speed": 0.71,
      "wind_deg": 179,
      "wind_gust": 2.77,
      "weather": [
        {
          "id": 802,
          "main": "Clouds",
          "description": "scattered clouds",
          "icon": "03d"
        }
      ],
      "pop": 0
    },
    {
      "dt": 1626800400,
      "temp": 29.08,
      "feels_like": 30.86,
      "pressure": 1010,
      "humidity": 58,
      "dew_point": 19.98,
      "uvi": 0.23,
      "clouds": 50,
      "visibility": 10000,
      "wind_speed": 0.46,
      "wind_deg": 22,
      "wind_gust": 2.68,
      "weather": [
        {
          "id": 802,
          "main": "Clouds",
          "description": "scattered clouds",
          "icon": "03d"
        }
      ],
      "pop": 0
    },
    {
      "dt": 1626804000,
      "temp": 28.3,
      "feels_like": 30.03,
      "pressure": 1010,
      "humidity": 61,
      "dew_point": 20.06,
      "uvi": 0,
      "clouds": 51,
      "visibility": 10000,
      "wind_speed": 1.75,
      "wind_deg": 331,
      "wind_gust": 4.5,
      "weather": [
        {
          "id": 803,
          "main": "Clouds",
          "description": "broken clouds",
          "icon": "04d"
        }
      ],
      "pop": 0
    },
    {
      "dt": 1626807600,
      "temp": 27.2,
      "feels_like": 28.58,
      "pressure": 1012,
      "humidity": 63,
      "dew_point": 19.55,
      "uvi": 0,
      "clouds": 10,
      "visibility": 10000,
      "wind_speed": 2.35,
      "wind_deg": 343,
      "wind_gust": 4.61,
      "weather": [
        {
          "id": 800,
          "main": "Clear",
          "description": "clear sky",
          "icon": "01n"
        }
      ],
      "pop": 0
    },
    {
      "dt": 1626811200,
      "temp": 25.93,
      "feels_like": 26.25,
      "pressure": 1014,
      "humidity": 64,
      "dew_point": 18.4,
      "uvi": 0,
      "clouds": 13,
      "visibility": 10000,
      "wind_speed": 3.5,
      "wind_deg": 1,
      "wind_gust": 6,
      "weather": [
        {
          "id": 801,
          "main": "Clouds",
          "description": "few clouds",
          "icon": "02n"
        }
      ],
      "pop": 0
    },
    {
      "dt": 1626814800,
      "temp": 25.16,
      "feels_like": 25.51,
      "pressure": 1014,
      "humidity": 68,
      "dew_point": 18.66,
      "uvi": 0,
      "clouds": 9,
      "visibility": 10000,
      "wind_speed": 3.77,
      "wind_deg": 4,
      "wind_gust": 6.91,
      "weather": [
        {
          "id": 800,
          "main": "Clear",
          "description": "clear sky",
          "icon": "01n"
        }
      ],
      "pop": 0
    },
    {
      "dt": 1626818400,
      "temp": 24.76,
      "feels_like": 25.09,
      "pressure": 1014,
      "humidity": 69,
      "dew_point": 18.45,
      "uvi": 0,
      "clouds": 6,
      "visibility": 10000,
      "wind_speed": 3.36,
      "wind_deg": 351,
      "wind_gust": 6.56,
      "weather": [
        {
          "id": 800,
          "main": "Clear",
          "description": "clear sky",
          "icon": "01n"
        }
      ],
      "pop": 0
    },
    {
      "dt": 1626822000,
      "temp": 24.62,
      "feels_like": 24.94,
      "pressure": 1014,
      "humidity": 69,
      "dew_point": 18.41,
      "uvi": 0,
      "clouds": 5,
      "visibility": 10000,
      "wind_speed": 3.64,
      "wind_deg": 333,
      "wind_gust": 6.97,
      "weather": [
        {
          "id": 800,
          "main": "Clear",
          "description": "clear sky",
          "icon": "01n"
        }
      ],
      "pop": 0
    },
    {
      "dt": 1626825600,
      "temp": 24.49,
      "feels_like": 24.77,
      "pressure": 1014,
      "humidity": 68,
      "dew_point": 18.02,
      "uvi": 0,
      "clouds": 4,
      "visibility": 10000,
      "wind_speed": 3.82,
      "wind_deg": 328,
      "wind_gust": 6.69,
      "weather": [
        {
          "id": 800,
          "main": "Clear",
          "description": "clear sky",
          "icon": "01n"
        }
      ],
      "pop": 0
    },
    {
      "dt": 1626829200,
      "temp": 24.4,
      "feels_like": 24.62,
      "pressure": 1014,
      "humidity": 66,
      "dew_point": 17.48,
      "uvi": 0,
      "clouds": 0,
      "visibility": 10000,
      "wind_speed": 3.7,
      "wind_deg": 322,
      "wind_gust": 6.06,
      "weather": [
        {
          "id": 800,
          "main": "Clear",
          "description": "clear sky",
          "icon": "01n"
        }
      ],
      "pop": 0
    },
    {
      "dt": 1626832800,
      "temp": 24.27,
      "feels_like": 24.4,
      "pressure": 1014,
      "humidity": 63,
      "dew_point": 16.71,
      "uvi": 0,
      "clouds": 0,
      "visibility": 10000,
      "wind_speed": 3.53,
      "wind_deg": 319,
      "wind_gust": 5.19,
      "weather": [
        {
          "id": 800,
          "main": "Clear",
          "description": "clear sky",
          "icon": "01n"
        }
      ],
      "pop": 0
    },
    {
      "dt": 1626836400,
      "temp": 24.09,
      "feels_like": 24.17,
      "pressure": 1014,
      "humidity": 62,
      "dew_point": 16.21,
      "uvi": 0,
      "clouds": 0,
      "visibility": 10000,
      "wind_speed": 3.37,
      "wind_deg": 316,
      "wind_gust": 4.43,
      "weather": [
        {
          "id": 800,
          "main": "Clear",
          "description": "clear sky",
          "icon": "01n"
        }
      ],
      "pop": 0
    },
    {
      "dt": 1626840000,
      "temp": 24.09,
      "feels_like": 24.12,
      "pressure": 1014,
      "humidity": 60,
      "dew_point": 15.88,
      "uvi": 0,
      "clouds": 0,
      "visibility": 10000,
      "wind_speed": 3.29,
      "wind_deg": 317,
      "wind_gust": 4.55,
      "weather": [
        {
          "id": 800,
          "main": "Clear",
          "description": "clear sky",
          "icon": "01d"
        }
      ],
      "pop": 0
    },
    {
      "dt": 1626843600,
      "temp": 24.89,
      "feels_like": 24.87,
      "pressure": 1015,
      "humidity": 55,
      "dew_point": 15.09,
      "uvi": 0.46,
      "clouds": 0,
      "visibility": 10000,
      "wind_speed": 3.19,
      "wind_deg": 318,
      "wind_gust": 6.47,
      "weather": [
        {
          "id": 800,
          "main": "Clear",
          "description": "clear sky",
          "icon": "01d"
        }
      ],
      "pop": 0
    },
    {
      "dt": 1626847200,
      "temp": 26.42,
      "feels_like": 26.42,
      "pressure": 1015,
      "humidity": 46,
      "dew_point": 13.53,
      "uvi": 1.46,
      "clouds": 0,
      "visibility": 10000,
      "wind_speed": 4.11,
      "wind_deg": 327,
      "wind_gust": 7.05,
      "weather": [
        {
          "id": 800,
          "main": "Clear",
          "description": "clear sky",
          "icon": "01d"
        }
      ],
      "pop": 0
    },
    {
      "dt": 1626850800,
      "temp": 27.76,
      "feels_like": 27.66,
      "pressure": 1015,
      "humidity": 43,
      "dew_point": 13.91,
      "uvi": 3.13,
      "clouds": 0,
      "visibility": 10000,
      "wind_speed": 4.46,
      "wind_deg": 331,
      "wind_gust": 6.63,
      "weather": [
        {
          "id": 800,
          "main": "Clear",
          "description": "clear sky",
          "icon": "01d"
        }
      ],
      "pop": 0
    },
    {
      "dt": 1626854400,
      "temp": 28.92,
      "feels_like": 28.8,
      "pressure": 1015,
      "humidity": 43,
      "dew_point": 14.69,
      "uvi": 5.29,
      "clouds": 0,
      "visibility": 10000,
      "wind_speed": 4.22,
      "wind_deg": 329,
      "wind_gust": 6,
      "weather": [
        {
          "id": 800,
          "main": "Clear",
          "description": "clear sky",
          "icon": "01d"
        }
      ],
      "pop": 0
    },
    {
      "dt": 1626858000,
      "temp": 30.06,
      "feels_like": 29.88,
      "pressure": 1015,
      "humidity": 41,
      "dew_point": 15.14,
      "uvi": 7.45,
      "clouds": 0,
      "visibility": 10000,
      "wind_speed": 4.18,
      "wind_deg": 336,
      "wind_gust": 5.62,
      "weather": [
        {
          "id": 800,
          "main": "Clear",
          "description": "clear sky",
          "icon": "01d"
        }
      ],
      "pop": 0
    },
    {
      "dt": 1626861600,
      "temp": 31.17,
      "feels_like": 30.85,
      "pressure": 1015,
      "humidity": 38,
      "dew_point": 14.85,
      "uvi": 9.03,
      "clouds": 0,
      "visibility": 10000,
      "wind_speed": 4.51,
      "wind_deg": 342,
      "wind_gust": 5.74,
      "weather": [
        {
          "id": 800,
          "main": "Clear",
          "description": "clear sky",
          "icon": "01d"
        }
      ],
      "pop": 0
    },
    {
      "dt": 1626865200,
      "temp": 31.73,
      "feels_like": 31.42,
      "pressure": 1014,
      "humidity": 37,
      "dew_point": 14.54,
      "uvi": 9.49,
      "clouds": 0,
      "visibility": 10000,
      "wind_speed": 5.25,
      "wind_deg": 346,
      "wind_gust": 5.97,
      "weather": [
        {
          "id": 800,
          "main": "Clear",
          "description": "clear sky",
          "icon": "01d"
        }
      ],
      "pop": 0
    },
    {
      "dt": 1626868800,
      "temp": 32.02,
      "feels_like": 31.64,
      "pressure": 1014,
      "humidity": 36,
      "dew_point": 13.95,
      "uvi": 8.72,
      "clouds": 0,
      "visibility": 10000,
      "wind_speed": 6.35,
      "wind_deg": 352,
      "wind_gust": 6.69,
      "weather": [
        {
          "id": 800,
          "main": "Clear",
          "description": "clear sky",
          "icon": "01d"
        }
      ],
      "pop": 0
    },
    {
      "dt": 1626872400,
      "temp": 32.02,
      "feels_like": 31.35,
      "pressure": 1014,
      "humidity": 34,
      "dew_point": 13.39,
      "uvi": 6.93,
      "clouds": 0,
      "visibility": 10000,
      "wind_speed": 7.34,
      "wind_deg": 358,
      "wind_gust": 8.55,
      "weather": [
        {
          "id": 800,
          "main": "Clear",
          "description": "clear sky",
          "icon": "01d"
        }
      ],
      "pop": 0
    },
    {
      "dt": 1626876000,
      "temp": 31.8,
      "feels_like": 30.95,
      "pressure": 1014,
      "humidity": 33,
      "dew_point": 12.94,
      "uvi": 4.7,
      "clouds": 0,
      "visibility": 10000,
      "wind_speed": 7.28,
      "wind_deg": 358,
      "wind_gust": 8.64,
      "weather": [
        {
          "id": 800,
          "main": "Clear",
          "description": "clear sky",
          "icon": "01d"
        }
      ],
      "pop": 0
    },
    {
      "dt": 1626879600,
      "temp": 31.25,
      "feels_like": 30.43,
      "pressure": 1014,
      "humidity": 34,
      "dew_point": 12.65,
      "uvi": 2.61,
      "clouds": 0,
      "visibility": 10000,
      "wind_speed": 7.43,
      "wind_deg": 358,
      "wind_gust": 9.2,
      "weather": [
        {
          "id": 800,
          "main": "Clear",
          "description": "clear sky",
          "icon": "01d"
        }
      ],
      "pop": 0
    },
    {
      "dt": 1626883200,
      "temp": 30.63,
      "feels_like": 29.73,
      "pressure": 1014,
      "humidity": 34,
      "dew_point": 12.43,
      "uvi": 1.1,
      "clouds": 0,
      "visibility": 10000,
      "wind_speed": 7.3,
      "wind_deg": 359,
      "wind_gust": 9.57,
      "weather": [
        {
          "id": 800,
          "main": "Clear",
          "description": "clear sky",
          "icon": "01d"
        }
      ],
      "pop": 0
    },
    {
      "dt": 1626886800,
      "temp": 29.84,
      "feels_like": 29.1,
      "pressure": 1014,
      "humidity": 36,
      "dew_point": 12.91,
      "uvi": 0.31,
      "clouds": 0,
      "visibility": 10000,
      "wind_speed": 6.15,
      "wind_deg": 358,
      "wind_gust": 9.11,
      "weather": [
        {
          "id": 800,
          "main": "Clear",
          "description": "clear sky",
          "icon": "01d"
        }
      ],
      "pop": 0
    },
    {
      "dt": 1626890400,
      "temp": 28.57,
      "feels_like": 28.26,
      "pressure": 1014,
      "humidity": 41,
      "dew_point": 13.73,
      "uvi": 0,
      "clouds": 0,
      "visibility": 10000,
      "wind_speed": 4.83,
      "wind_deg": 356,
      "wind_gust": 8.25,
      "weather": [
        {
          "id": 800,
          "main": "Clear",
          "description": "clear sky",
          "icon": "01d"
        }
      ],
      "pop": 0
    },
    {
      "dt": 1626894000,
      "temp": 27.31,
      "feels_like": 27.39,
      "pressure": 1015,
      "humidity": 45,
      "dew_point": 14.12,
      "uvi": 0,
      "clouds": 0,
      "visibility": 10000,
      "wind_speed": 4.57,
      "wind_deg": 0,
      "wind_gust": 8.47,
      "weather": [
        {
          "id": 800,
          "main": "Clear",
          "description": "clear sky",
          "icon": "01n"
        }
      ],
      "pop": 0
    },
    {
      "dt": 1626897600,
      "temp": 26.88,
      "feels_like": 26.98,
      "pressure": 1015,
      "humidity": 44,
      "dew_point": 13.43,
      "uvi": 0,
      "clouds": 0,
      "visibility": 10000,
      "wind_speed": 4.83,
      "wind_deg": 2,
      "wind_gust": 9.14,
      "weather": [
        {
          "id": 800,
          "main": "Clear",
          "description": "clear sky",
          "icon": "01n"
        }
      ],
      "pop": 0
    },
    {
      "dt": 1626901200,
      "temp": 26.61,
      "feels_like": 26.61,
      "pressure": 1015,
      "humidity": 45,
      "dew_point": 13.37,
      "uvi": 0,
      "clouds": 0,
      "visibility": 10000,
      "wind_speed": 5.26,
      "wind_deg": 358,
      "wind_gust": 9.94,
      "weather": [
        {
          "id": 800,
          "main": "Clear",
          "description": "clear sky",
          "icon": "01n"
        }
      ],
      "pop": 0
    },
    {
      "dt": 1626904800,
      "temp": 26.32,
      "feels_like": 26.32,
      "pressure": 1015,
      "humidity": 48,
      "dew_point": 14.31,
      "uvi": 0,
      "clouds": 0,
      "visibility": 10000,
      "wind_speed": 5.45,
      "wind_deg": 349,
      "wind_gust": 10.27,
      "weather": [
        {
          "id": 800,
          "main": "Clear",
          "description": "clear sky",
          "icon": "01n"
        }
      ],
      "pop": 0
    },
    {
      "dt": 1626908400,
      "temp": 26.15,
      "feels_like": 26.15,
      "pressure": 1015,
      "humidity": 51,
      "dew_point": 15.14,
      "uvi": 0,
      "clouds": 0,
      "visibility": 10000,
      "wind_speed": 5.78,
      "wind_deg": 343,
      "wind_gust": 10.79,
      "weather": [
        {
          "id": 800,
          "main": "Clear",
          "description": "clear sky",
          "icon": "01n"
        }
      ],
      "pop": 0
    },
    {
      "dt": 1626912000,
      "temp": 26.17,
      "feels_like": 26.17,
      "pressure": 1015,
      "humidity": 49,
      "dew_point": 14.51,
      "uvi": 0,
      "clouds": 0,
      "visibility": 10000,
      "wind_speed": 6.55,
      "wind_deg": 342,
      "wind_gust": 12.05,
      "weather": [
        {
          "id": 800,
          "main": "Clear",
          "description": "clear sky",
          "icon": "01n"
        }
      ],
      "pop": 0
    },
    {
      "dt": 1626915600,
      "temp": 26.12,
      "feels_like": 26.12,
      "pressure": 1014,
      "humidity": 50,
      "dew_point": 14.52,
      "uvi": 0,
      "clouds": 0,
      "visibility": 10000,
      "wind_speed": 6.76,
      "wind_deg": 344,
      "wind_gust": 12.45,
      "weather": [
        {
          "id": 800,
          "main": "Clear",
          "description": "clear sky",
          "icon": "01n"
        }
      ],
      "pop": 0
    },
    {
      "dt": 1626919200,
      "temp": 25.87,
      "feels_like": 25.84,
      "pressure": 1014,
      "humidity": 51,
      "dew_point": 14.82,
      "uvi": 0,
      "clouds": 0,
      "visibility": 10000,
      "wind_speed": 6.66,
      "wind_deg": 348,
      "wind_gust": 12.28,
      "weather": [
        {
          "id": 800,
          "main": "Clear",
          "description": "clear sky",
          "icon": "01n"
        }
      ],
      "pop": 0
    },
    {
      "dt": 1626922800,
      "temp": 25.57,
      "feels_like": 25.57,
      "pressure": 1014,
      "humidity": 53,
      "dew_point": 15.17,
      "uvi": 0,
      "clouds": 0,
      "visibility": 10000,
      "wind_speed": 6.53,
      "wind_deg": 352,
      "wind_gust": 11.88,
      "weather": [
        {
          "id": 800,
          "main": "Clear",
          "description": "clear sky",
          "icon": "01n"
        }
      ],
      "pop": 0
    },
    {
      "dt": 1626926400,
      "temp": 25.22,
      "feels_like": 25.29,
      "pressure": 1015,
      "humidity": 57,
      "dew_point": 16.03,
      "uvi": 0,
      "clouds": 0,
      "visibility": 10000,
      "wind_speed": 6.05,
      "wind_deg": 353,
      "wind_gust": 10.98,
      "weather": [
        {
          "id": 800,
          "main": "Clear",
          "description": "clear sky",
          "icon": "01d"
        }
      ],
      "pop": 0
    },
    {
      "dt": 1626930000,
      "temp": 25.64,
      "feels_like": 25.83,
      "pressure": 1015,
      "humidity": 60,
      "dew_point": 17.08,
      "uvi": 0.44,
      "clouds": 0,
      "visibility": 10000,
      "wind_speed": 5.78,
      "wind_deg": 351,
      "wind_gust": 10.07,
      "weather": [
        {
          "id": 800,
          "main": "Clear",
          "description": "clear sky",
          "icon": "01d"
        }
      ],
      "pop": 0
    },
    {
      "dt": 1626933600,
      "temp": 26.87,
      "feels_like": 27.6,
      "pressure": 1015,
      "humidity": 55,
      "dew_point": 16.96,
      "uvi": 1.37,
      "clouds": 0,
      "visibility": 10000,
      "wind_speed": 5.45,
      "wind_deg": 350,
      "wind_gust": 8.3,
      "weather": [
        {
          "id": 800,
          "main": "Clear",
          "description": "clear sky",
          "icon": "01d"
        }
      ],
      "pop": 0
    },
    {
      "dt": 1626937200,
      "temp": 28.12,
      "feels_like": 28.68,
      "pressure": 1015,
      "humidity": 51,
      "dew_point": 16.7,
      "uvi": 2.94,
      "clouds": 0,
      "visibility": 10000,
      "wind_speed": 5.13,
      "wind_deg": 346,
      "wind_gust": 7.16,
      "weather": [
        {
          "id": 800,
          "main": "Clear",
          "description": "clear sky",
          "icon": "01d"
        }
      ],
      "pop": 0
    },
    {
      "dt": 1626940800,
      "temp": 29.18,
      "feels_like": 29.53,
      "pressure": 1015,
      "humidity": 47,
      "dew_point": 16.36,
      "uvi": 4.99,
      "clouds": 0,
      "visibility": 10000,
      "wind_speed": 5.08,
      "wind_deg": 342,
      "wind_gust": 6.64,
      "weather": [
        {
          "id": 800,
          "main": "Clear",
          "description": "clear sky",
          "icon": "01d"
        }
      ],
      "pop": 0
    },
    {
      "dt": 1626944400,
      "temp": 30.13,
      "feels_like": 30.35,
      "pressure": 1015,
      "humidity": 44,
      "dew_point": 16.2,
      "uvi": 7.02,
      "clouds": 0,
      "visibility": 10000,
      "wind_speed": 5.2,
      "wind_deg": 344,
      "wind_gust": 6.38,
      "weather": [
        {
          "id": 800,
          "main": "Clear",
          "description": "clear sky",
          "icon": "01d"
        }
      ],
      "pop": 0
    },
    {
      "dt": 1626948000,
      "temp": 30.88,
      "feels_like": 31.05,
      "pressure": 1015,
      "humidity": 42,
      "dew_point": 15.93,
      "uvi": 8.49,
      "clouds": 0,
      "visibility": 10000,
      "wind_speed": 5.31,
      "wind_deg": 350,
      "wind_gust": 6.41,
      "weather": [
        {
          "id": 800,
          "main": "Clear",
          "description": "clear sky",
          "icon": "01d"
        }
      ],
      "pop": 0
    },
    {
      "dt": 1626951600,
      "temp": 31.63,
      "feels_like": 31.59,
      "pressure": 1015,
      "humidity": 39,
      "dew_point": 15.48,
      "uvi": 8.92,
      "clouds": 0,
      "visibility": 10000,
      "wind_speed": 5.52,
      "wind_deg": 1,
      "wind_gust": 6.45,
      "weather": [
        {
          "id": 800,
          "main": "Clear",
          "description": "clear sky",
          "icon": "01d"
        }
      ],
      "pop": 0
    },
    {
      "dt": 1626955200,
      "temp": 32.2,
      "feels_like": 31.88,
      "pressure": 1014,
      "humidity": 36,
      "dew_point": 14.73,
      "uvi": 8.2,
      "clouds": 0,
      "visibility": 10000,
      "wind_speed": 5.89,
      "wind_deg": 11,
      "wind_gust": 7.02,
      "weather": [
        {
          "id": 800,
          "main": "Clear",
          "description": "clear sky",
          "icon": "01d"
        }
      ],
      "pop": 0
    },
    {
      "dt": 1626958800,
      "temp": 32.2,
      "feels_like": 32.04,
      "pressure": 1014,
      "humidity": 37,
      "dew_point": 14.66,
      "uvi": 6.59,
      "clouds": 0,
      "visibility": 10000,
      "wind_speed": 6.2,
      "wind_deg": 11,
      "wind_gust": 7.44,
      "weather": [
        {
          "id": 800,
          "main": "Clear",
          "description": "clear sky",
          "icon": "01d"
        }
      ],
      "pop": 0
    }
  ],
  "daily": [
    {
      "dt": 1626775200,
      "sunrise": 1626752240,
      "sunset": 1626805006,
      "moonrise": 1626792900,
      "moonset": 1626738360,
      "moon_phase": 0.36,
      "temp": {
        "day": 28.63,
        "min": 22.08,
        "max": 29.8,
        "night": 25.16,
        "eve": 29.45,
        "morn": 22.08
      },
      "feels_like": {
        "day": 28.98,
        "night": 25.51,
        "eve": 31.48,
        "morn": 22.43
      },
      "pressure": 1011,
      "humidity": 48,
      "dew_point": 16.32,
      "wind_speed": 4.6,
      "wind_deg": 326,
      "wind_gust": 6.91,
      "weather": [
        {
          "id": 500,
          "main": "Rain",
          "description": "light rain",
          "icon": "10d"
        }
      ],
      "clouds": 0,
      "pop": 0.88,
      "rain": 0.2,
      "uvi": 8.81
    },
    {
      "dt": 1626861600,
      "sunrise": 1626838691,
      "sunset": 1626891362,
      "moonrise": 1626883740,
      "moonset": 1626827340,
      "moon_phase": 0.4,
      "temp": {
        "day": 31.17,
        "min": 24.09,
        "max": 32.02,
        "night": 26.61,
        "eve": 30.63,
        "morn": 24.09
      },
      "feels_like": {
        "day": 30.85,
        "night": 26.61,
        "eve": 29.73,
        "morn": 24.12
      },
      "pressure": 1015,
      "humidity": 38,
      "dew_point": 14.85,
      "wind_speed": 7.43,
      "wind_deg": 358,
      "wind_gust": 9.94,
      "weather": [
        {
          "id": 800,
          "main": "Clear",
          "description": "clear sky",
          "icon": "01d"
        }
      ],
      "clouds": 0,
      "pop": 0,
      "uvi": 9.49
    },
    {
      "dt": 1626948000,
      "sunrise": 1626925143,
      "sunset": 1626977716,
      "moonrise": 1626974160,
      "moonset": 1626916920,
      "moon_phase": 0.44,
      "temp": {
        "day": 30.88,
        "min": 25.22,
        "max": 32.2,
        "night": 25.56,
        "eve": 30.68,
        "morn": 25.22
      },
      "feels_like": {
        "day": 31.05,
        "night": 25.27,
        "eve": 30.25,
        "morn": 25.29
      },
      "pressure": 1015,
      "humidity": 42,
      "dew_point": 15.93,
      "wind_speed": 7.36,
      "wind_deg": 8,
      "wind_gust": 12.45,
      "weather": [
        {
          "id": 800,
          "main": "Clear",
          "description": "clear sky",
          "icon": "01d"
        }
      ],
      "clouds": 0,
      "pop": 0,
      "uvi": 8.92
    },
    {
      "dt": 1627034400,
      "sunrise": 1627011595,
      "sunset": 1627064068,
      "moonrise": 1627063980,
      "moonset": 1627007100,
      "moon_phase": 0.47,
      "temp": {
        "day": 29.45,
        "min": 24.03,
        "max": 31.46,
        "night": 27.61,
        "eve": 30.89,
        "morn": 24.03
      },
      "feels_like": {
        "day": 28.62,
        "night": 28.1,
        "eve": 30.63,
        "morn": 23.77
      },
      "pressure": 1014,
      "humidity": 35,
      "dew_point": 11.44,
      "wind_speed": 3.32,
      "wind_deg": 230,
      "wind_gust": 5.65,
      "weather": [
        {
          "id": 800,
          "main": "Clear",
          "description": "clear sky",
          "icon": "01d"
        }
      ],
      "clouds": 0,
      "pop": 0,
      "uvi": 9.57
    },
    {
      "dt": 1627120800,
      "sunrise": 1627098048,
      "sunset": 1627150418,
      "moonrise": 1627153200,
      "moonset": 1627097760,
      "moon_phase": 0.5,
      "temp": {
        "day": 30.04,
        "min": 25.77,
        "max": 31.26,
        "night": 27.56,
        "eve": 31.26,
        "morn": 25.77
      },
      "feels_like": {
        "day": 29.51,
        "night": 28.13,
        "eve": 31.91,
        "morn": 25.63
      },
      "pressure": 1014,
      "humidity": 38,
      "dew_point": 13.44,
      "wind_speed": 3.82,
      "wind_deg": 207,
      "wind_gust": 4.11,
      "weather": [
        {
          "id": 800,
          "main": "Clear",
          "description": "clear sky",
          "icon": "01d"
        }
      ],
      "clouds": 0,
      "pop": 0,
      "uvi": 9.41
    },
    {
      "dt": 1627207200,
      "sunrise": 1627184502,
      "sunset": 1627236767,
      "moonrise": 1627241880,
      "moonset": 1627188480,
      "moon_phase": 0.55,
      "temp": {
        "day": 30.47,
        "min": 26.48,
        "max": 31.37,
        "night": 27.63,
        "eve": 31.32,
        "morn": 26.48
      },
      "feels_like": {
        "day": 30.8,
        "night": 28.55,
        "eve": 32,
        "morn": 26.48
      },
      "pressure": 1014,
      "humidity": 44,
      "dew_point": 15.91,
      "wind_speed": 4.17,
      "wind_deg": 181,
      "wind_gust": 4.07,
      "weather": [
        {
          "id": 800,
          "main": "Clear",
          "description": "clear sky",
          "icon": "01d"
        }
      ],
      "clouds": 0,
      "pop": 0,
      "uvi": 10
    },
    {
      "dt": 1627293600,
      "sunrise": 1627270957,
      "sunset": 1627323114,
      "moonrise": 1627330140,
      "moonset": 1627279140,
      "moon_phase": 0.58,
      "temp": {
        "day": 31.14,
        "min": 26.47,
        "max": 31.81,
        "night": 27.26,
        "eve": 30.52,
        "morn": 26.47
      },
      "feels_like": {
        "day": 32.65,
        "night": 28.84,
        "eve": 32.59,
        "morn": 26.47
      },
      "pressure": 1014,
      "humidity": 49,
      "dew_point": 18.26,
      "wind_speed": 6.01,
      "wind_deg": 161,
      "wind_gust": 7.63,
      "weather": [
        {
          "id": 800,
          "main": "Clear",
          "description": "clear sky",
          "icon": "01d"
        }
      ],
      "clouds": 6,
      "pop": 0.08,
      "uvi": 10
    },
    {
      "dt": 1627380000,
      "sunrise": 1627357411,
      "sunset": 1627409460,
      "moonrise": 1627418100,
      "moonset": 1627369680,
      "moon_phase": 0.62,
      "temp": {
        "day": 31.21,
        "min": 26.06,
        "max": 32.97,
        "night": 28.26,
        "eve": 31.02,
        "morn": 26.41
      },
      "feels_like": {
        "day": 32.77,
        "night": 29.6,
        "eve": 33.06,
        "morn": 26.41
      },
      "pressure": 1014,
      "humidity": 49,
      "dew_point": 17.62,
      "wind_speed": 4.54,
      "wind_deg": 171,
      "wind_gust": 5.3,
      "weather": [
        {
          "id": 800,
          "main": "Clear",
          "description": "clear sky",
          "icon": "01d"
        }
      ],
      "clouds": 8,
      "pop": 0,
      "uvi": 10
    }
  ]
}