import { createSlice } from '@reduxjs/toolkit'

export const weatherSlice = createSlice({
  name: 'weather',
  initialState: {
  },
  reducers: {
    update: (state, action) => {
      state.data = action.payload;
      // console.log(state.data)
    },
    updateForecast: (state, action) => {
      state.forecast = action.payload;
      // console.log(state.forecast)
    }
  },
})

export const { update, updateForecast } = weatherSlice.actions;

export default weatherSlice.reducer;